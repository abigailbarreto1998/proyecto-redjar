export interface User {
    id: number,
    name: string, 
    username: string,
    city: string,
    address: {city: string}
}